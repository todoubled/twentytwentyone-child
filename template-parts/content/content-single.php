<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since 1.0.0
 * 
 */
$post_id = get_the_ID();
$featured_image = wp_get_attachment_image( get_post_thumbnail_id($post_id), '1568', false); //get_post_thumbnail_id($post_id), 'full'
$featured_image_alt = get_post_meta($post_id, '_wp_attachment_image_alt', true);
$category = get_the_category(get_the_ID());

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <?php if ($featured_image) { ?>
    <div class="container-full">
      <div class="post-standard -full">
        <div class="post-standard__banner">
          <div class="post-standard__banner__image"><?php echo $featured_image ?></div>
          <div class="post-standard__banner__content">
            <div class="post-card -center">
              <div class="card__content">
                <h5 class="card__content-category"><?php echo $category[0]->name ?></h5>
                <h1 class="card__content-title"><?php the_title() ?></h1>
                <div class="card__content-info">
                  <div class="info__time">
                    <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="clock" class="svg-inline--fa fa-clock fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm61.8-104.4l-84.9-61.7c-3.1-2.3-4.9-5.9-4.9-9.7V116c0-6.6 5.4-12 12-12h32c6.6 0 12 5.4 12 12v141.7l66.8 48.6c5.4 3.9 6.5 11.4 2.6 16.8L334.6 349c-3.9 5.3-11.4 6.5-16.8 2.6z"></path></svg>
                    <p><?php the_date() ?></p>
                  </div>
                  <?php 
                    $comment_count = get_comment_count(get_the_ID())['approved'];
                    if ($comment_count) { ?>
                    <div class="info__comment">
                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="comment" class="svg-inline--fa fa-comment fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M256 32C114.6 32 0 125.1 0 240c0 49.6 21.4 95 57 130.7C44.5 421.1 2.7 466 2.2 466.5c-2.2 2.3-2.8 5.7-1.5 8.7S4.8 480 8 480c66.3 0 116-31.8 140.6-51.4 32.7 12.3 69 19.4 107.4 19.4 141.4 0 256-93.1 256-208S397.4 32 256 32z"></path></svg>
                      <p><?php echo $comment_count ?></p>
                    </div>
                  <?php } ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php } else { ?>
    <header class="entry-header alignwide">
      <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header>
  <?php } ?>
	<div class="entry-content">
		<?php
		the_content();

		wp_link_pages(
			array(
				'before'   => '<nav class="page-links" aria-label="' . esc_attr__( 'Page', 'twentytwentyone' ) . '">',
				'after'    => '</nav>',
				/* translators: %: page number. */
				'pagelink' => esc_html__( 'Page %', 'twentytwentyone' ),
			)
		);
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer default-max-width">
		<?php twenty_twenty_one_entry_meta_footer(); ?>
	</footer><!-- .entry-footer -->

	<?php if ( ! is_singular( 'attachment' ) ) : ?>
		<?php get_template_part( 'template-parts/post/author-bio' ); ?>
	<?php endif; ?>

</article><!-- #post-${ID} -->
